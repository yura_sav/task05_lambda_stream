package com.epam.lambda.model.task04.uniquewords;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
//to do
public class UniqueWord {
    private static List<String> list = new ArrayList<>();
    private static Scanner scanner = new Scanner(System.in);

    public List<String> getList() {
        addElements();
        return list;

    }

    private void addElements() {
        System.out.println("Enter word");
        while (true) {
            String word = scanner.nextLine();
            if (word.isEmpty()) {
                break;
            }
            list.add(word);
        }
    }
}